<?php
/*
  Template Name: Calendrier
*/
?>
<?php get_header(); ?>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/fullcalendar@5/main.min.css">

<section class="container">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<div class="row">
				<div class="col-sm">
					<h2><?php the_title(); ?></h2>
					<?php the_content(); ?>
				</div>
			</div>
	<?php endwhile;
	endif; ?>
</section>

<!-- Modal -->
<div class="modal fade" id="fc_modal" tabindex="-1" role="dialog" aria-labelledby="fullcalendar" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header justify-content-center">
        <h4 id="fc_titre" class="modal-title" id="fullcalendar">Modal title</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div id="fc_content" class="modal-body">
        <p id="fc_lieu" class="text-center font-italic"></p>
        <p id="fc_desc" class="text-justify"></p>
      </div>
      <div class="modal-footer justify-content-center">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
      </div>
    </div>
  </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/fullcalendar@5/main.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/fullcalendar@5/locales-all.min.js"></script>

<?php get_footer(); ?>