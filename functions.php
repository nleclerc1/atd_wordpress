<?php

// Pour les menus BootStrap
require_once('bs4navwalker.php');
// Pour la pagination
require_once('bs4pagination.php');

// Ajouter la prise en charge des images mises en avant
add_theme_support('post-thumbnails');

// Ajouter automatiquement le titre du site dans l'en-tête du site
add_theme_support('title-tag');

// Ajoute le menu pour personnaliser l'image du header
function themename_custom_header_setup() {
    $args = array(
        'default-image'      => get_template_directory_uri() . '/img/header.jpg',
        'default-text-color' => '000',
        'width'              => 1000,
        'height'             => 400,
        'flex-width'         => true,
        'flex-height'        => true
    );
    add_theme_support( 'custom-header', $args );
}
add_action( 'after_setup_theme', 'themename_custom_header_setup' );


// Les menus
register_nav_menus(array(
    'main' => 'Menu Principal'
));

// Enregistrement des javascripts et feuilles de styles
function aikitaido_register_assets()
{

    // On annule l'inscription du jQuery de WP
    wp_deregister_script('jquery');

    // jQuery
    wp_enqueue_script(
        'jquery',
        'https://code.jquery.com/jquery-3.5.1.min.js',
        false,
        '3.5.1',
        false
    );

    // Bootstrap 4
    wp_enqueue_script(
        'bootstrap',
        'https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js',
        array('jquery'),
        '4.4.1',
        true
    );

    // Déclarer le JS
    wp_enqueue_script(
        'aikitaido',
        get_template_directory_uri() . '/js/script.js',
        array('jquery'),
        '1.1',
        true
    );

    // Déclarer le style bootstrap
    wp_enqueue_style(
        'bootstrap',
        'https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css',
        array(),
        '4.4.1'
    );

    // Déclarer le style bootstrap
    wp_enqueue_style(
        'ekko-lightbox',
        'https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css',
        array('bootstrap'),
        '5.3.0'
    );

    // Font awesome
    wp_enqueue_style(
        'fontawesome',
        'https://use.fontawesome.com/releases/v5.7.2/css/all.css',
        array(),
        '5.7.2'
    );

    // Style du site
    wp_enqueue_style(
        'aikitaido',
        get_template_directory_uri() . '/scss/aikitaido.min.css',
        array('bootstrap', 'fontawesome'),
        '1.0'
    );

    // Déclarer style.css à la racine du thème en dernier pour surcharger le style du site
    wp_enqueue_style(
        'aikitaido_theme',
        get_stylesheet_uri(),
        array(),
        '1.0'
    );
}
add_action('wp_enqueue_scripts', 'aikitaido_register_assets');


// Enregistrement des zones de widget dans le footer
function register_widget_areas()
{

    register_sidebar(array(
        'name'          => 'Widget pied de page: zone 1',
        'id'            => 'footer_area_one',
        'description'   => 'Pied de page 1',
        'before_widget' => '<section class="footer-area footer-area-one">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>',
    ));

    register_sidebar(array(
        'name'          => 'Widget pied de page: zone 2',
        'id'            => 'footer_area_two',
        'description'   => 'Pied de page 2',
        'before_widget' => '<section class="footer-area footer-area-two">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>',
    ));

    register_sidebar(array(
        'name'          => 'Widget pied de page: zone 3',
        'id'            => 'footer_area_three',
        'description'   => 'Pied de page 3',
        'before_widget' => '<section class="footer-area footer-area-three">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>',
    ));
}
add_action('widgets_init', 'register_widget_areas');

/**
 * Filter the except length to 20 words.
 *
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
 */
function wpdocs_custom_excerpt_length($length)
{
    return 8;
}
add_filter('excerpt_length', 'wpdocs_custom_excerpt_length', 999);


function wpm_custom_post_type()
{

    // On rentre les différentes dénominations de notre custom post type qui seront affichées dans l'administration
    $labels = array(
        // Le nom au pluriel
        'name'                => _x('Animateurs', 'Post Type General Name'),
        // Le nom au singulier
        'singular_name'       => _x('Animateur', 'Post Type Singular Name'),
        // Le libellé affiché dans le menu
        'menu_name'           => __('Animateurs'),
        // Les différents libellés de l'administration
        'all_items'           => __('Tous les animateurs'),
        'view_item'           => __('Voir les animateurs'),
        'add_new_item'        => __('Ajouter une nouveau animateur'),
        'add_new'             => __('Ajouter'),
        'edit_item'           => __('Editer un animateur'),
        'update_item'         => __('Modifier un animateur'),
        'search_items'        => __('Rechercher un animateur'),
        'not_found'           => __('Non trouvée'),
        'not_found_in_trash'  => __('Non trouvée dans la corbeille'),
    );

    // On peut définir ici d'autres options pour notre custom post type

    $args = array(
        'label'               => __('Animateurs'),
        'description'         => __('Tous les animateurs'),
        'labels'              => $labels,
        'menu_icon'           => 'dashicons-format-gallery',
        // On définit les options disponibles dans l'éditeur de notre custom post type ( un titre, un auteur...)
        'supports'            => array('title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields',),
        /* 
		* Différentes options supplémentaires
		*/
        'show_in_rest'        => true,
        'hierarchical'        => false,
        'public'              => true,
        'has_archive'         => true,
        'rewrite'             => array('slug' => 'animateurs'),

    );

    // On enregistre notre custom post type qu'on nomme ici "serietv" et ses arguments
    register_post_type('animateurs', $args);
}

add_action('init', 'wpm_custom_post_type', 0);

//On crée 3 taxonomies personnalisées: Année, Réalisateurs et Catégories de série.

function wpm_add_taxonomies()
{
    // Diplome 

    $labels_cat = array(
        'name'                       => _x('Diplômes', 'taxonomy general name'),
        'singular_name'              => _x('Diplôme', 'taxonomy singular name'),
        'search_items'               => __('Rechercher une diplôme'),
        'popular_items'              => __('Diplômes populaires'),
        'all_items'                  => __('Tous les diplômes'),
        'edit_item'                  => __('Editer un diplôme'),
        'update_item'                => __('Mettre à jour un diplôme'),
        'add_new_item'               => __('Ajouter une nouveau diplôme'),
        'new_item_name'              => __('Nom du nouveau diplôme'),
        'add_or_remove_items'        => __('Ajouter ou supprimer un diplôme'),
        'choose_from_most_used'      => __('Choisir parmi les diplômes les plus utilisées'),
        'not_found'                  => __('Pas de diplômes trouvées'),
        'menu_name'                  => __('Diplômes'),
    );

    $args_cat_serie = array(
        // Si 'hierarchical' est défini à true, notre taxonomie se comportera comme une catégorie standard
        'hierarchical'          => true,
        'labels'                => $labels_cat,
        'show_ui'               => true,
        'show_in_rest'            => true,
        'show_admin_column'     => true,
        'query_var'             => true,
        'rewrite'               => array('slug' => 'diplomes'),
    );

    register_taxonomy('diplomes', 'animateurs', $args_cat_serie);
}

add_action('init', 'wpm_add_taxonomies', 0);

//Masquer la version de WordPress
function cs_remove_version() {
	return '';
}
add_filter('the_generator', 'cs_remove_version');
