<?php get_header(); ?>

<section class="container">

    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <div class="row">
                <div class="col-sm">
                    <article class="post">
                        <h3><?php the_title(); ?></h3>

                        <div class="row">
                            <div class="col-sm">
                                <p id="publish">Publié le <?php the_date(); ?></p>
                            </div>
                            <div class="col-sm" id="tags">
                                <?php
                                $id = get_the_id();
                                $tags = get_the_tags($id);
                                $html = '';
                                foreach ($tags as $tag) {
                                    $tag_link = get_tag_link($tag->term_id);
                                    $html .= "<a href='{$tag_link}' class='badge badge-primary'>{$tag->name}</a>\n";
                                }
                                $categories = get_the_terms($id, 'category');
                                foreach ($categories as $cat) {
                                    $tag_link = get_tag_link($cat->term_id);
                                    $html .= "<a href='{$cat_link}' class='badge badge-info'>{$cat->name}</a>\n";
                                }
                                echo $html;
                                ?>
                            </div>
                        </div>

                        <div class="post__content">
                            <?php the_content(); ?>
                        </div>
                    </article>
                </div>
            </div>
    <?php endwhile;
    endif; ?>
    <nav>
        <ul class="pagination justify-content-around">
            <li class="page-item">
                <?php
                $html = get_previous_post_link('%link', '&laquo; %title');
                echo str_replace('<a', '<a class="page-link"', $html);
                ?>
            </li>
            <li class="page-item">
                <?php
                $html = get_next_post_link('%link', '%title &raquo;');
                echo str_replace('<a', '<a class="page-link"', $html);
                ?>
            </li>
        </ul>
    </nav>
</section>


<?php get_footer(); ?>