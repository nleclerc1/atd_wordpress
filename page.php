<?php get_header(); ?>

<section class="container">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<div class="row">
				<div class="col-sm">
					<h2><?php the_title(); ?></h2>

					<?php the_content(); ?>
				</div>
			</div>
	<?php endwhile;
	endif; ?>
</section>

<?php get_footer(); ?>