<?php

/**
 * Template for displaying search forms in Aiki Tai Do
 *
 * @package WordPress
 * @subpackage Aiki Tai Do
 * @since Aiki Tai Do 1.1
 */
?>
<form id="searchform" class="searchform" role="search" method="get" action="<?php echo esc_url(home_url('/')); ?>">
    <div class="input-group mb-3">
        <input id="s" name="s" type="text" class="form-control" placeholder="Chercher...">
        <div class="input-group-append">
            <button class="btn btn-primary" id="searchsubmit" type="submit" name="submit" >Chercher</button>
        </div>
    </div>
</form>