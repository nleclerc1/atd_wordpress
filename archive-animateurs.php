<?php get_header(); ?>

<section class="container">
    <div class="row">
        <div class="col-sm">
            <h2>Les animateurs</h2>
        </div>
    </div>

    <div class="row">
<?php
// le svg
$diplomes = file_get_contents(get_template_directory() . "/img/diplomes.svg");
$count_id = 0;
$cqp_lien = "";

// Retirer la limite du nombre de post, pour avoir tous les animateurs sur la page
global $wp_query;
$args = array_merge( $wp_query->query_vars, array('posts_per_page' => 1000, 'orderby'=> 'title', 'order' => 'ASC'));
query_posts( $args );

if (have_posts()) {
    while (have_posts()) {
        the_post();

        $count_id++;

        // Extraction de la position dans le titre pour trier
        $title = explode('.', get_the_title(), 2);
        if (count($title) == 1) {
            $t = $title[0];
            $title = array(99999 + $count_id, $t);
        }

        // Calcule de l'id du SVG pour le style
        $svgid = 'id="diplome_' . $count_id . '" class="card-img-top"';
        $svg = str_replace('id="REPLACE_WITH_PHP"', $svgid, $diplomes);

        // Trouver les propriétés à changer
        $terms = get_the_terms($post->ID, 'diplomes');
        $props = array();
        $props["ceinture_bord"] = "black";
        $props["ceinture_couleur"] = "white";
        $props["ceinture_bande"] = "rgba(0,0,0,0)";
        $props["cqp_lien"] = "";
        foreach ($terms as $t) {
            $json = json_decode($t->description, true);
            $props = array_merge($props, $json);
        }

        // Le lien vers lequel un click sur le cqp doit aller
        if (! empty($props["cqp_lien"])) {
            $cqp_lien = $props["cqp_lien"];
        }
?>
        <div class="col-lg-4 col-md-6 col-sm-12 mb-3" id="<?php echo $title[0]; ?>">
            <div class="card">
                <div id="box_<?=$count_id?>">
                    <img id="animateur_<?=$count_id?>" class="card-img-top" src="<?php the_post_thumbnail_url(); ?>" alt="image de la carte">
                    <?=$svg?>
                </div>
                <style type="text/css">
                    #diplome_<?=$count_id?> .ceinture .couleur {
                        fill: <?=$props["ceinture_couleur"]?> !important;
                        stroke: <?=$props["ceinture_bord"]?> !important;
                    }
                    #diplome_<?=$count_id?> .ceinture .bande {
                        stroke: <?=$props["ceinture_bande"]?> !important;
                    }
                <?php if (isset($props["ecusson_couleur"])) { ?>
                    #diplome_<?=$count_id?> .ecusson .couleur {
                        fill: <?=$props["ecusson_couleur"]?> !important;
                    }
                <?php } else { ?>
                    #diplome_<?=$count_id?> .ecusson {
                        display: none;
                    }
                <?php } ?>
                <?php if (! isset($props["cqp"])) { ?>
                    #diplome_<?=$count_id?> .cqp {
                        display: none;
                    }
                <?php } ?>
                #box_<?=$count_id?> {
                        margin-bottom: 3rem;
                        position: relative;
                    }
                    #diplome_<?=$count_id?> {
                        position: absolute;
                        left: 0px;
                        top: 86%;
                        z-index: 1;
                    }
                </style>
                <div class="card-body">
                    <h5 class="card-title"><?php echo $title[1]; ?></h5>
                    <?php the_content(); ?>
                </div>
            </div>
        </div>
<?php    
    }
}
?>
    </div>
</section>

<script>
<?php if (! empty($cqp_lien)) { ?>
    $(document).ready(function(){
        $(".cqp").mouseover(function() {$(".cqp").css("cursor", "pointer");});
        $(".cqp").click(function() { window.location="<?=$cqp_lien?>"; });
    });
<?php } ?>
</script>

<?php get_footer(); ?>