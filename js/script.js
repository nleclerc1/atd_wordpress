// Menu animation
$(window).scroll(function() {
    if ($(this).scrollTop() > 30) {
        if (!$('.navbar').hasClass('opaque')) {
            $('.navbar').addClass('opaque');
        }
    } else {
        if ($('.navbar').hasClass('opaque')) {
            $('.navbar').removeClass('opaque');
        }
    }
});

// Modifier les tableaux produit par WP avec Bootstrap
$(function() {
    $("table").each(function(index, element) {
        $(element).addClass("table table-striped table-bordered");
        $(element).find("thead").addClass("thead-dark");
    });
});