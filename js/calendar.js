// <div id="calendar"></div>
// <script>
document.addEventListener('DOMContentLoaded', function() {
    var calendarEl = document.getElementById('calendar');

    var calendar = new FullCalendar.Calendar(calendarEl, {
        height: 'auto',
        themeSystem: 'bootstrap',
        locale: 'fr',
        initialView: 'listMonth',
        headerToolbar: {
            left: 'prev',
            center: 'title',
            right: 'next'
        },
        footerToolbar: {
            left: 'listMonth',
            center: 'today',
            right: 'dayGridMonth'
        },
        dayMaxEvents: true, // allow "more" link when too many events
        googleCalendarApiKey: 'AIzaSyAJhVsrIZsAp4RosDmgIFJ_RmqG6eWZef0',
        events: {
            googleCalendarId: 'c_jpfivr4vcffej0pm1jnjcc9p84@group.calendar.google.com'
        },
        eventClick: function(info) {
            info.jsEvent.preventDefault();
            $("#fc_titre").text(info.event.title);
            $("#fc_lieu").text(info.event.extendedProps.location);
            $("#fc_desc").html(info.event.extendedProps.description);
            $('#fc_modal').modal('show');
            // if (info.event.url) window.open(info.event.url);
        },
        eventDataTransform: function(event) {
            var days = Math.floor((Date.parse(event.end) - Date.parse(event.start)) / 86400000);
            if (days > 1) event.display = 'background';
            return event;
        }
    });
    calendar.render();
});
// </script>