<footer id="section-about">
    <div class="container">
        <div class="row">
            <?php if (is_active_sidebar('footer_area_one')) : ?>
                <div class="col-sm">
                    <?php dynamic_sidebar('footer_area_one'); ?>
                </div>
            <?php endif; ?>
            <?php if (is_active_sidebar('footer_area_two')) : ?>
                <div class="col-sm">
                    <?php dynamic_sidebar('footer_area_two'); ?>
                </div>
            <?php endif; ?>
            <?php if (is_active_sidebar('footer_area_three')) : ?>
                <div class="col-sm">
                    <?php dynamic_sidebar('footer_area_three'); ?>
                </div>
            <?php endif; ?>

        </div>
    </div>
</footer>

<?php wp_footer(); ?>

</body>
</html>