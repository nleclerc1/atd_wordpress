<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php
// Gestion de l'image d'entête par Wordpress
global $wp;
$current_slug = add_query_arg( array(), $wp->request );
// par defaut, pas d'image pour l'entête
$header_display = "none";
$header_class = "";
$header_image = "";
// somme nous, dans / qui retourne "" ?
if (strlen($current_slug) == 0){
    // Oui donc il faut afficher l'image
    $header_class = "premiere_page";
    $header_display = "block";
    $the_image = get_header_image();
    $header_image = "background: url('".
        ((strlen($the_image)==0)?get_template_directory_uri() . '/img/header.jpg':$the_image).
        "');background-size: cover;background-position: top;";;
}
?>
    <header class="<?=$header_class;?>" style="<?=$header_image;?>">
    
        <div class="container">

            <nav class="navbar navbar-dark menu-color navbar-expand-lg fixed-top">
                <div class="container">
                    <a href="/" class="navbar-brand">
                        <img src="<?php echo get_site_icon_url($url = get_theme_file_uri('screenshot.png')); ?>" /> Aïki-Taï-Dô
                    </a>
                    <button class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <?php
                    wp_nav_menu([
                        'menu'            => 'main',
                        'theme_location'  => 'main',
                        'container'       => 'div',
                        'container_id'    => 'navbarCollapse',
                        'container_class' => 'collapse navbar-collapse',
                        'menu_id'         => false,
                        'menu_class'      => 'navbar-nav ml-auto',
                        'depth'           => 2,
                        'fallback_cb'     => 'bs4navwalker::fallback',
                        'walker'          => new bs4navwalker()
                    ]);
                    ?>
            </nav>
            <div style="display: <?=$header_display;?>;">
                <h1><?php bloginfo('name'); ?></h1>
                <?php
                $descriptions = get_bloginfo('description');
                $explode_desc = explode("|", $descriptions);
                $i = 0;
                foreach($explode_desc as &$desc) {
                    if ($i == 0) {
                        echo "<h2>".$desc."</h2>";
                    } else {
                        echo "<h2 class='smaller'>".$desc."</h2>";
                    }
                    ++$i;
                }
                ?>
                <a href="#section-about" id="arrow"><i class="fas fa-arrow-down"></i></a>
            </div>

        </div>

    </header>