<?php get_header(); ?>

<?php
if (is_category()) {
    $title = "Catégorie : " . single_tag_title('', false);
} elseif (is_tag()) {
    $title = "Étiquette : " . single_tag_title('', false);
} elseif (is_search()) {
    $title = "Vous avez recherché : " . get_search_query();
} else {
    $title = '';
}
?>

<section class="container">
    <?php
    // 1. On définit les arguments pour définir ce que l'on souhaite récupérer
    $args = array(
        'post_type' => 'page',
        'pagename' => 'annonce'
    );

    // 2. On exécute la WP Query
    $my_query = new WP_Query( $args );

    // 3. On lance la boucle !
    if( $my_query->have_posts() ) : while( $my_query->have_posts() ) : $my_query->the_post();
    ?>
        <div class="row">
                <div class="col-sm">
        <?php the_content(); ?>
                </div>
        </div>
    <?php
    endwhile;
    endif;

    // 4. On réinitialise à la requête principale (important)
    wp_reset_postdata();
    ?>

    <div class="row">
        <div class="col-sm">
            <p><?php echo $title; ?></p>

            <?php if (have_posts()) { ?>
                <div class="row">
                    <?php while (have_posts()) {
                        the_post(); ?>
                        <div class="col-lg-4 col-md-6 col-sm-12 mb-3">
                            <div class="card">
                                <div class="embed-responsive embed-responsive-16by9">
                                    <?php if (has_post_thumbnail()) : ?>
                                        <img class="card-img-top embed-responsive-item" src="<?php the_post_thumbnail_url('medium'); ?>" />
                                    <?php else : ?>
                                        <img class="card-img-top embed-responsive-item" src="<?php echo get_theme_file_uri('img/default.jpg'); ?>" />
                                    <?php endif; ?>
                                </div>
                                <div class="card-body">
                                    <h5 class="card-title"><?php the_title(); ?></h5>
                                    <p class="card-text"><?php echo wp_strip_all_tags(get_the_excerpt(), true); ?></p>
                                    <a href="<?php the_permalink(); ?>" class="btn btn-primary stretched-link float-right">Lire la suite</a>
                                </div>
                                <div class="card-footer text-muted d-flex justify-content-center">
                                    <small>Publié le <?php the_time(get_option('date_format')); ?></small>
                                </div>
                            </div>
                        </div>
                    <?php }; ?>
                </div>
            <?php } ?>

            
        </div>
    </div>

    <?php bs4pagination(); ?>

</section>

<?php get_footer(); ?>